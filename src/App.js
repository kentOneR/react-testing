import React, { Component } from 'react';

import GuessedWords from './GuessedWords';
import Congrats from './Congrats';

class App extends Component {

	render() {
		return (
			<div className="App container">
				<h1>Jotto Demo</h1>
				<Congrats success={false} />
				<GuessedWords guessedWords={[]} />
			</div>
		);
	}

}

export default App;
