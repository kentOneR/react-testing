import React from 'react';
import {shallow} from 'enzyme';

import {findByTestAttr, checkProp} from '../test/utils';

import Congrats from './Congrats';

const defaultProps = {success: false}

const setup = (props = {}) => {
    const setupProps = {...defaultProps, ...props};
    return shallow(<Congrats {...setupProps} />);
};

test('renders without error', () => {
    const wrapper = setup();
    const component = findByTestAttr(wrapper, 'component-congrats');

    expect(component.length).toBe(1);
});

test('renders no text when props "success" is false', () => {
    const wrapper = setup({success: false});
    const component = findByTestAttr(wrapper, 'component-congrats');

    expect(component.text()).toBe('');
});

test('renders non-empty congrats message when props "success" is true', () => {
    const wrapper = setup({success: true});
    const message = findByTestAttr(wrapper, 'congrats-message');

    expect(message.text().length).not.toBe(0);
});

test('not throw warning with expect props', () => {
    const expectedProps = {success: false};
    checkProp(Congrats, expectedProps);
});