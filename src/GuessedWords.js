import React from 'react';
import PropTypes from 'prop-types';

const GuessedWords= (props) => {

    let content;
    if (props.guessedWords.length === 0) {
        content = (
            <span data-test="guessed-instructions">
            </span>
        );
    } else {

        const guessedWordsRow = props.guessedWords.map((word, index) => (
            <tr key={index} data-test="guessed-word">
                <td>{word.guessedWord}</td>
                <td>{word.letterMatchCount}</td>
            </tr>
        ));

        content = (
            <div data-test="guessed-words">
                <h3>Guesses Words</h3>
                <table className="table table-sm">
                    <thead className="thead-light">
                        <tr>
                            <th>Guess</th>
                            <th>Matching Letters</th>
                        </tr>
                    </thead>
                    <tbody>
                        {guessedWordsRow}
                    </tbody>
                </table>
            </div>
        );
    }

    return (
        <div data-test="component-guessed-words">
            {content}
        </div>
    );;
};

GuessedWords.propTypes = {
    guessedWords: PropTypes.arrayOf(
        PropTypes.shape({
            guessedWord: PropTypes.string.isRequired,
            letterMatchCount: PropTypes.number.isRequired
        })
    ).isRequired       
};

export default GuessedWords;