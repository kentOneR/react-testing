import React from 'react';
import {shallow} from 'enzyme';
import {findByTestAttr, checkProp} from '../test/utils';

import GuessedWords  from './GuessedWords';

const defaultProps = {
    guessedWords: [
        {
            guessedWord: 'train',
            letterMatchCount: 3
        }
    ]
};


/**
 * 
 * @param {object} props 
 */
const setup = (props = {}) => {
    const setupProps = {
        ...defaultProps,
        ...props
    };

    return shallow(<GuessedWords {...setupProps} />);
};

test('not throw warning with expect props', () => {
    checkProp(GuessedWords, defaultProps);
});

describe('if no word guessed', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = setup({guessedWords: []});
    });

    test('renders without error', () => {
        const component = findByTestAttr(wrapper, 'component-guessed-words');
        expect(component.length).toBe(1);
    });

    test('renders instruction to guess a word', () => {
        const instructionsEl = findByTestAttr(wrapper, 'guessed-instructions');
        expect(instructionsEl.length).toBe(1);
    });
});

describe('if word guessed', () => {

    const guessedWords = [
        {guessedWord: 'train', letterMatchCount: 3},
        {guessedWord: 'party', letterMatchCount: 1},
        {guessedWord: 'agile', letterMatchCount: 5}
    ];

    let wrapper;

    beforeEach(() => {
        wrapper = setup({guessedWords});
    });

    test('renders without error', () => {
        const component = findByTestAttr(wrapper, 'component-guessed-words');
        expect(component.length).toBe(1);
    });

    test('renders guessed word section', () => {
        const guessedWordsEl = findByTestAttr(wrapper, 'guessed-words');
        expect(guessedWordsEl.length).toBe(1);
    });

    test('correct number of guessed words', () => {
        const guessedWordEl = findByTestAttr(wrapper, 'guessed-word');
        expect(guessedWordEl.length).toBe(guessedWords.length);
    });
});