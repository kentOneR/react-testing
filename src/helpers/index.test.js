import {getLetterMatchCount} from './';

describe('getLetterMatchCount', () => {

    const secretWord = 'party';

    test('returns correct count if no matching letter', () => {
        const letterMatchCount = getLetterMatchCount('bones', secretWord);
        expect(letterMatchCount).toBe(0);
    });

    test('returns correct count if 3 matching letter', () => {
        const letterMatchCount = getLetterMatchCount('train', secretWord);
        expect(letterMatchCount).toBe(3);
    });

    test('returns correct count if duplicate matching letter', () => {
        const letterMatchCount = getLetterMatchCount('parka', secretWord);
        expect(letterMatchCount).toBe(3);
    });
});