import { actionTypes } from '../actions';
import successReducers from './success';

test('returns default initial state of "false" when no action', () => {
    const newState = successReducers(undefined, {});
    expect(newState).toBe(false);
});

test('returns state of "true" on action type "CORRECT_GUESS"', () => {
    const newState = successReducers(undefined, {type: actionTypes.CORRECT_GUESS});
    expect(newState).toBe(true);
});

