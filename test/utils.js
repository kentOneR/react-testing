import CheckPropTypes from 'check-prop-types';
import {createStore, applyMiddleware} from 'redux';

import rootReducer from '../src/reducers';
import {middlewares} from '../src/configureStore';

/**
 * 
 * @param {object} initialState 
 */
export const storeFactory = (initialState) => {
    const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
    return createStoreWithMiddleware(rootReducer, initialState);
};

/**
 * 
 * @param {ShallowWrapper} wrapper 
 * @param {string} value
 */
export const findByTestAttr = (wrapper, value) => wrapper.find(`[data-test="${value}"]`);

export const checkProp = (component, confirmingProps) => {
    const propError = CheckPropTypes(
        component.propTypes,
        confirmingProps,
        'prop',
        component.name
    );

    expect(propError).toBeUndefined();
};